<?php

function sdj_current_cat_info($attr, $content=""){
	  
  if (is_archive() ){
		wp_enqueue_style("sdj-main-style" ,plugin_dir_url( __FILE__ ).'asset/css/main.css');
		global $wp_query;
		$cat = $wp_query->get_queried_object();
		 
		$thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true ); 
		 $cat_name= $cat->name; 
		//var_dump($cat_desc);
		 $cat_desc = $cat->description;
	    $image = wp_get_attachment_url( $thumbnail_id ); 
		
   if ( ! empty($attr['image']) ){ 
			 $result= "<div class='cat-image-gradient'><img src='".$image."' class='cat-image'  /></div>";
			 return $result;
		}
	
	elseif ( ! empty($attr['title']) ){ 
		 $result= "<div class='cat-title'> <h1 >".$cat_name."</h1></div>";
		 return $result; 
	}
	
	elseif ( ! empty($attr['desc']) ){ 
		 $result= "<div class='cat-desc' ><p>".$cat_desc."</p></div>";
		 return $result; 
	}  
   
}
	   
  }
  add_shortcode('sdj-current-cat-info', 'sdj_current_cat_info');
  
  ?>