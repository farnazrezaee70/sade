<?php 

	global $woocommerce;
	global $product;
	global $post; 
	global $wpdb;
 	class PostList{
		public $postType;
		public $filterShow;
		public $showType;
		public $postNumber;
		PUBLIC $catId;
		public $sortPost;
		public $taxonamy;
		public $args;
		public $postsQuery;
		public $argsQuery;
		public $pageId;
		
		public function __construct($postType="product",$catId =0,$postNumber =12){ 
			 $this->postType=$postType;
			 $this->catId=$catId;
			 $this->sortPost='desc';
			 $this->postNumber=$postNumber;
			 if($this->postType=='product'){
				$this-> taxonamy='product_cat';
			 }else{
				$this-> taxonamy='category';
				 
			 }  
		}
	 	 public function showPosts($showType="list",$shortcodeId,$filterShow){  
			$this->showType=$showType;  
			$this->filterShow=$filterShow;
			 
			if($showType=="list"){  
					return $this->postListMaker($shortcodeId); 
			
			}elseif($showType=="create-list"){
				return $this->postListMaker($shortcodeId); 
			
			}elseif($showType=="carousel"){
					return $this->postCarouselMaker($shortcodeId);
			} 
	   
		} 
		public function getCategory(){ 
			$all_subCat=array();  
			 $cat_type=gettype($this->catId);
			if($cat_type!='array'){ 
			 $args = array(
				'hierarchical' => 1,
				'show_option_none' => '',
				'hide_empty' => 0,
				'parent' => $this->catId,
				'taxonomy' => $this-> taxonamy
				); 
				 $subcats = get_categories($args);   
					foreach($subcats as $subcat){ 
						array_push( $all_subCat,$subcat ->cat_ID); 
					}  
					array_push( $all_subCat,$this->catId); 
					
				}else{  
					$catgorys=$this->catId; 
						foreach($catgorys as $catgory){ 
								$args = array(
									'hierarchical' => 1,
									'show_option_none' => '',
									'hide_empty' => 0,
									'parent' => $catgory,
									'taxonomy' => $this-> taxonamy
									); 
									$subcats = get_categories($args); 
									//var_dump($subcats);
									foreach($subcats as $subcat){ 
										array_push( $all_subCat,$subcat ->cat_ID);
									} 
									array_push( $all_subCat,$catgory); 
					 
								} 
				}
			 $this->catId=$all_subCat; 
			 return  $this->catId;
		}
		public function readyQuery($sortStyle,$pageId){ 
		$this->pageId=$pageId;
			global $paged; 
			$currentPage = get_queried_object('paged'); 
			
			$this->sortPost=$sortStyle;
			$all_sub_cats=$this->catId;
				if($this->postType=='product'){
					$order_by="meta_key";
					$order_by_value="_price";
				}else{
					$order_by='orderby';
					$order_by="Date";
				}			 
			if($this->catId !=0){ 
				if($this->pageId==0){
				$args=array( 
					   'post_status' => 'publish',
						'post_type' => $this->postType ,
						'posts_per_page'=>$this->postNumber,
						'paged' => $paged, 
						'order'          => $sort_style,
						$order_by       => $order_by_value,
						'tax_query' => array(
						
											'relation' => 'OR',
											array(
												'taxonomy'      => $this->taxonamy,
												'field' => 'term_id',   
												'terms'         =>$all_sub_cats 
											)
										)); 
										$this->argsQuery	=$args;	
					
				}else{
					
						$args=array( 
							   'post_status' => 'publish',
							   'offset'=>$this->postNumber*$this->pageId,
								'post_type' => $this->postType ,
								'posts_per_page'=>$this->postNumber,
								'paged' => $paged, 
								'order'          => $sort_style,
								$order_by       => $order_by_value,
								'tax_query' => array(
								
													'relation' => 'OR',
													array(
														'taxonomy'      => $this->taxonamy,
														'field' => 'term_id',   
														'terms'         =>$all_sub_cats 
													)
												)); 
										$this->argsQuery	=$args;	
					
				}								
			}else{  			
				$args=array( 
				   'post_status' => 'publish',
					'post_type' => $this->postType ,
					'offset'=>$this->postNumber*$this->pageId,
					'post_per_page'=>$this->postNumber,
					'order'          => $sort_style,
					$order_by       => $order_by_value,
					);	 
				} 
				
				
				if($this->postType=='product'){ 
					$posts_query= new WP_Query($args); 
				}else{
					$posts_query= new WP_Query( $args );
				}
				$this->postsQuery=$posts_query; 
				return $this->postsQuery;
		}
	 	 public function postCarouselMaker ($shortcodeId){ 
				
				$posts_query=$this->postsQuery;  
				$result='<div class="owl-carousel owl-theme" id="'.$shortcodeId.'">';
				while ( $posts_query->have_posts() ) : $posts_query->the_post();  
						$result.='<div class="item"><a href="'.get_the_permalink().'">'.get_the_post_thumbnail() ;
						$result.='<div class="sdj-slider-title"><h4>'.get_the_title().'</h4></div>';  
						$result.='<div class="sdj-slider-price"><p>'.wc_get_product( )->price.'₺</p> </div>';  
						$result.='</div>'; 
				endwhile; 
				wp_reset_postdata(); 
			$result.='</div> ';  
			return $result; 
		}
		public function postListMaker($shortcodeId){ 
			
				$posts_query=$this->postsQuery;  
				$all_subcats=$this->catId; 
				
				$result="<div id='".$shortcodeId."' >";
				
				if($this->postType=='product'){ 
					if($this->filterShow==true&&$this->showType=='list'	){ 
						$result.="<div id='filter-result-container' class='hidden'><span class='remove-filter'><i class=' fas fa-times'></i></span><div id='filter-result'  ></div></div>";
						$result.="<div class='filter-header-container'><div class='filter-header'>";
						$result.="<div class='filter-box-container' ><div class='filter-box' id='filter-box-sort'><i class='elementor-icons-manager__tab__item__icon fas fa-sort-amount-down-alt'></i><p> sort</p> </div><div class='filter-box-content'  id='filter-box-sort-content' style='grid-template-columns: repeat(2,1fr);'><p>Most Price</p> <p>Less Price</p></div></div>";
						$result.=" <div class='filter-box-container' ><div class='filter-box'  id='filter-box-filter'><i class='elementor-icons-manager__tab__item__icon fas fa-filter'></i><p>filter</p></div><div class='filter-box-content'  id='filter-box-cat-content'  style='grid-template-columns: repeat(".$cat_count.",1fr);'>";
							foreach($all_subcats as $subcat){
									if( $term = get_term_by( 'id', $subcat, 'product_cat' ) ){
											$result.='<p class="'.$subcat.'">'. $term->name.'</p>';
										}
							}
				
						$result.="</div></div>";
						$result.="<div id='filter-save hidden'></div>";
						$result.="</div></div>"; 
					}elseif($this->filterShow==true&&$this->showType=='create-list'	){
						$filters.="<div id='filter-result-container' class='hidden'><span class='remove-filter'><i class=' fas fa-times'></i></span><div id='filter-result'  ></div></div>";
						
						$filters.="<div class='filters-container'>";
						$filters.="<div class='filters'>"; 
						$attributes = wc_get_attribute_taxonomies();
						$array_length=count($all_subcats)-1;
						$main_cat=$all_subcats[$array_length].'<br>';
						$attribute_name_montur=array();
						$attribute_name_diomand=array('sertifica','renk','berraklik','kesim','pirlanta');
							
							
						foreach ($attributes as $attribute) {
							$category_diamond = get_term_by( 'slug', 'diamond', 'product_cat' );
							$category_montur = get_term_by( 'slug', 'montur', 'product_cat' ); 
								if($main_cat==$category_diamond->term_id){
									$choices_name=array();
									if(in_array($attribute->attribute_name, $attribute_name_diomand)){
											array_push($choices_name,$attribute->attribute_name);
										
												$choices=$attribute->attribute_terms = get_terms(array(
																				'taxonomy' => 'pa_'.$attribute->attribute_name,
																				'hide_empty' => false,
																				));
											$filters.="<div class='filter'>";
											$filters.="<div class='filter-box'><p>".$attribute->attribute_name ."</p><i class='fas fa-chevron-down'></i></div>";
											$choices=$attribute->attribute_terms ; 
												foreach($choices as $choice){
													$name=strtoupper($choice->name);
													$filters.="<div class='filter-items'><p class='filter-item'>".$name."</p></div>"; 
												}
									  
											$filters.="</div>";
										}
									}else if($main_cat==$category_montur->term_id) {
										$choices_name=array();
										if(!in_array($attribute->attribute_name, $attribute_name_diomand)){
											
											array_push($choices_name,$attribute->attribute_name);
											array_push($attribute_name_montur,$attribute->attribute_name);
										
											$choices=$attribute->attribute_terms = get_terms(array(
																				'taxonomy' => 'pa_'.$attribute->attribute_name,
																				'hide_empty' => false,
																				));
											$filters.="<div class='filter'>";									
											$filters.="<div class='filter-box'><p>".$attribute->attribute_name ."</p><i class='fas fa-chevron-down'></i></div>";
											$choices=$attribute->attribute_terms ; 
												foreach($choices as $choice){
													$name=strtoupper($choice->name);
													$filters.="<div class='filter-items'><p class='filter-item'>".$name."</p></div>"; 
												}
									  
											$filters.="</div>";																				
										}
									}
						} 
									  
						$filters.="</div>";
 
							 
								 
								
							 
							$result.=$filters;
					  } 
				}
				$result.="<ul class='".$this->postType."-list  sdj-list'>";  
				if($this->postType=='product'){
					 
				while ( $posts_query->have_posts() ) : $posts_query->the_post();  
					$result.="<li class='product-item'>
					 <div class='product-item-inside-item'>";
				   $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
					$result.="<img src=$image[0]><a class='product-item-link'  href=".get_the_permalink()."> <h4 class='product-item-title'>".get_the_title()."</h4></a>"; 
					if($this->showType=='list'){  
					$result.="<p class='product-item-price'>". wc_get_product( )->price."₺ </p></div></li>"; 
					}elseif($this->showType=='create-list'){ //href='?add-to-cart=".get_the_ID()."'
					$result.="<div class='sdj-product-detail'><div ><a class=' add_to_cart_button ajax_add_to_cart' href='javascript:void(0)'  data-product_id='".get_the_ID()."' data-quantity='1'>Add</a> "; 
					$result.="<p class='product-item-price'>". wc_get_product( )->price."₺ </p></div></div><div class='sdj-more-info'><a href=".get_the_permalink()."><span>More info</span></div></a></li>"; 
						
					}
				
				endwhile; 
				wp_reset_postdata(); 
				}
				
				elseif($this->postType=='post'){
				while ( $posts_query->have_posts() ) : $posts_query->the_post();  
					$result.="<li class='post-item'>";
				//	echo'lll1';
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
					
					$result.="<div class='post-img'><img src=".$image[0]."></div><div class='post-item-inside-item'><a class='post-item-link'  href='".get_the_permalink()."'> <h5 class='post-cat-title'>".get_the_category($post->ID)[0]->name."</h5>";
					
					$result.="<div class='author-box'><div class='post-author-img'><img src='".get_avatar_url(get_the_author_meta('ID'))."'></div> <div  class='post-author-name'><span>By </span><span class='name' >".get_the_author() ."</span> - <span>".get_the_date()."</span> </div></div>"; 
					
					$result.="<h4 class='post-item-title'>".get_the_title()."</h4></a>"; 
					
					$result.="<p class='post-excerpt'>".get_the_excerpt() ." </p></div>"; 
					$result.="<a class='post-btn' href='".get_the_permalink()."'> Read More </a></li>"; 
				endwhile; 
				wp_reset_postdata();
				}
				$result.="</ul></div>";  
				if( $posts_query->max_num_pages <= 1 ){
					  return $result;
				}
					global $wp; 
					 $max   = intval( $posts_query->max_num_pages );  
					$result.= '<div class="navigation  '.$this->postType.'-navigation"><ul style="grid-template-columns: repeat('.$max.',1fr);" >' ;
						 
        
								for($i=0;$i<$posts_query->max_num_pages;$i++)
								{
									$j=$i+1;
								   if($this->pageId==$i){
									$class='pagi active';
									
									}else{
										$class='pagi';
									} $result.='<li class="'.$class.'"  data-page="'.$i.'" data-total="'.$posts_query->max_num_pages.'">'.$j.' </li>';
							
								} 
					  
						
					  
						
					 
						$result.= '</ul></div>';  
						$result.= '</div>';  
						$result.= '</div>';
						return $result;
 			
		}

	}//end class
	
	
class CustomProduct{
	public $diomandName;
	public $monturName;
	public $diomandId;
	public $monturId;
	public $diomandGalleryId;
	public $monturGalleryId;
	public $galleryIds;
	
	
	public function __construct(){
		
		if ( WC()->cart->get_cart_contents_count() != 0 ) {	
			foreach (WC()->cart->get_cart() as  $cart_item ){//$cart_item_key =>
				 
					$product_id=$cart_item['product_id'];
					$product = new WC_product($product_id);
					
								
						foreach( get_the_terms( $cart_item['product_id'], 'product_cat' ) as $term ){
							if( $term->parent == 0 ){
								$parent_cat_id= $term->term_id; 
								$cat_term=  get_term_by( 'id', $parent_cat_id, 'product_cat' );
								 
								if(strtolower($cat_term->name)=='diamond'){
									$this->diomandGalleryIds = $product->get_gallery_image_ids(); 
									 $this-> diomandName= $product->get_title();
									 $this-> diomandId=$product->get_id();
									//print_r($diomand_gallery_ids);
									 //wc_display_product_attributes( $product );
									
									
								}
								if(strtolower($cat_term->name)=='montur'){
									$this->monturGalleryIds = $product->get_gallery_image_ids(); 
									 $this->monturName= $product->get_title();
									 $this->monturId=$product->get_id();
									
								}
								
							}
						}
			}
		}
				if(isset($this->monturGalleryIds)&&isset($this->diomandGalleryIds)!=0){
				$this->galleryIds=array_merge($this->monturGalleryIds,$this->diomandGalleryIds);
				 } 
	}
	public function customProductName(){
		global $product;
		 $montur_name=$this->monturName;
		  $diomand_name=$this->diomandName;
		  
		  $final_name= '<div class="custom-product-title"><h1>'.$diomand_name.' With '.$montur_name.'</h1></div>';
		  return $final_name;
	}
	public function customProductDetail(){
		
		 
		if(isset($this->diomandId)){$diomand = wc_get_product( $this->diomandId); wc_display_product_attributes( $diomand ); }
		if(isset($this->monturId )){$montur = wc_get_product($this->monturId); wc_display_product_attributes( $montur  );}
		 
		
	}
	public function customProductGallery(){
		//print_R($gallery_ids);
		$result= '';
		
		$result.='<div class="final-product"><div class="owl-carousel owl-theme" id="sync1">';
		if(isset($this->galleryIds)){
			foreach($this->galleryIds as $img_id){ 
				$img_src= wp_get_attachment_image_src($img_id);
				$result.='<div class="item ">';
					$result.='<img src='.$img_src[0].' >';
				$result.='</div>'; 
					
			} 
	}	
		$result.='</div><div id="sync2" class="owl-carousel owl-theme">';
		if(isset($this->galleryIds)){
			foreach($this->galleryIds as $img_id){
				$img_src= wp_get_attachment_image_src($img_id);
				$result.='<div class="item ">';
					$result.='<img src='.$img_src[0].' >';
				$result.='</div>';
			}
		}	
		$result.='</div></div>';
		return $result;
		
	}
	public function customProductPrice(){
	
		$diomand = wc_get_product( $this->diomandId);
		$doimond_price= $diomand ->price;
		
		$montur = wc_get_product($this->monturId);
		$montur_price= $montur->price;
		
		$whole_price=intval($montur_price)+intval($doimond_price);
		$whole_price_box="<div class='price-checkout-btn final-product'><div class='checkout-btn'><a href='#'> PAY </a></div><div class='price-btn'><p>".$whole_price." ₺ </p></div></div>";
		return $whole_price_box;
		
	}
	

}	



 ?>