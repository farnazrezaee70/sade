 <?php 
    function sdj_oposite_product_carousel(){
	 global $wpdb; 
	 global $product;
	 global $post;
	 if(is_single()){
		 global $post;
		$terms = get_the_terms( $post->ID, 'product_cat' ); 
		foreach ($terms as $term) {
			$parent_cat_id = $term->parent; 
			if($parent_cat_id ==0){
					$parent_term= get_term_by( 'id', $term->term_id, 'product_cat' ) ;
					$parent_name=$parent_term->name; 
					$parent_name=trim($parent_name);
					if(strtolower($parent_name)=='montur'){
						 $opposite_cat= get_term_by( 'name', 'diamond', 'product_cat' ) ;
						$opposite_cat_id=$opposite_cat->term_id; 
					}elseif(strtolower($parent_name)=='diamond'){
						
						 $opposite_cat= get_term_by( 'name', 'montur', 'product_cat' ) ;
						$opposite_cat_id=$opposite_cat->term_id; 
						
					}
			} 
		} 
 
	 } 
	 $oposite_post_carousel=new PostList('product',$opposite_cat_id,4); 
	 $oposite_post_carousel->getCategory();	  
	 $oposite_post_carousel->readyQuery('desc',0);  
	 $oposite_post_carousel = $oposite_post_carousel->showPosts('carousel','oposite_product',true); 
	 wp_enqueue_style("sdj-main-style" ,plugin_dir_url( __FILE__ ).'asset/css/main.css'); 
	 wp_enqueue_style( 'font-awesome-style', 'https://pro.fontawesome.com/releases/v5.10.0/css/all.css'  ); 
	 wp_enqueue_style("sdjowl-carousel-style" ,plugin_dir_url( __FILE__ )."asset/css/owl.carousel.min.css");
	 wp_enqueue_style("sdjowl-theme-carousel-style" ,plugin_dir_url( __FILE__ )."asset//css/owl.theme.default.min.css"); 
	 wp_enqueue_script("sdj-owl-carousel-script" ,plugin_dir_url( __FILE__ )."asset/js/owl.carousel.js"); 
	 wp_enqueue_script('sdj-oposite-product-slider', plugin_dir_url( __FILE__ ).'asset/js/oposite-product-slider.js');
	return $oposite_post_carousel;
  }  
   add_shortcode('sdj-oposite-product-carousel','sdj_oposite_product_carousel');
   
 ?>
