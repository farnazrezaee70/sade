<?php 

 /*
Plugin Name: sdj Tools
Plugin URI: #
Description: sdj Tools
Version: 1.0
Author: Farnaz Rezaee
Author URI: #
Text Domain:    sdj
License: sdj*/

define('gtdu',get_template_directory_uri());  
  
  function set_row_count_archive($query){
    if ($query->is_archive) {
            $query->set('posts_per_page', 15);
   }
    return $query;
}

function sdj_main_style() {
	wp_enqueue_style("sdj-main-style" ,plugin_dir_url( __FILE__ ).'asset/css/main.css'); 
}
add_action( 'wp_enqueue_scripts', 'sdj_main_style' );

/*defiend class */
require_once('inc/class.php');  

/*mega-menu*/
require_once('inc/mega-menu/mega-menu-controller-shortcode.php');    
//require_once
//
require_once('inc/archive-product-list/archive-product-list-ajax.php'); 
require_once('inc/archive-product-list/archive-product-list-shortcode.php');  


require_once('inc/blog/blog-shortcode.php'); 
require_once('inc/blog/blog-list-ajax.php'); 

require_once('inc/opposite-product-carousel-shortcode.php'); 
require_once('inc/category-shortcode.php'); 

require_once('inc/create-custom-product/create-custom-product-ajax.php'); 
require_once('inc/create-custom-product/create-custom-product-shortcode.php');  

require_once('inc/single-product-popup.php'); 
require_once('inc/product-show-attribute.php'); 


require_once('inc/woocomerce-add-cart/woocomerce-add-cart-validation.php'); 
require_once('inc/woocomerce-add-cart/woocomerce-add-cart-validation-ajax.php'); 



 
  